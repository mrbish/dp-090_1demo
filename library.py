#! /usr/bin/env python3
"""
************************************
Your favorite books always with you!
************************************
\n Commands:
\t help() - see all commands
\t genre_all() - all genres in your Library
\t genre_info('Your genre title') - all information about genre
\t genre_add('Your new genre title') - add new genre
\t genre_del('Your genre title') - delete genre and if your books
\n 
\t book_all() - all books in your Library
\t book_info('Your booke title') - all information about book
\t book_add('title', 'author', year, 'description', 'genre') - add new book 
\t (if don't know genre it'll automatically add "Interesting")
\t book_del(title) - delete book
\t book_genre_edit(title, new_genre) - title of genre, new_genre - new title
\t book_all_by_genre(genre_title) - all books by Genre (genre_title)
"""

import sys, pprint
from modules import (genre, \
                     book, \
                     id_generator)
from local_data import local_data
from views import (genre_view, \
                   book_view)
from db import in_file


def help_me():
    print(__doc__)


def genre_add(title):
    titles = [local_data.genre_dict[key].title for key, value in local_data.genre_dict.items()]
    if title in titles:
        print('You have this Genre in your Library')
    else:
        new_genre = genre.Genre(id_generator.generate_id(), title)
        local_data.genre_dict[new_genre.genre_id] = new_genre

        print('You have new Genre - {title} in your Library'.format(title=title))


def genre_del(title):
    del_item_id = None
    for key, value in local_data.genre_dict.items():
        if title == local_data.genre_dict[key].title:
            del_item_id = key
            break
    local_data.genre_dict.pop(del_item_id, None)
    for book in local_data.book_dict:
        if local_data.book_dict[book].genre == title:
            local_data.book_dict[book].genre = "Interesting"


def book_add(title, author, year, description, genre='Interesting'):
    if title in book_dict.keys():
        print('You have this Book in your Library')
    else:
        new_book = book.Book(id_generator.generate_id(), title, author, year, description, genre)
        if genre in genre_dict.keys():
            book_dict[new_book.title] = new_book
            print('You have new Book - {title} in your Library'.format(title=title))
        else:
            print('Wrong Genre! You can choose one of them:')
            genre_all()
            print('Or add new Genre with use this command \"genre_add(title)\"')


def book_del(title):
    book_dict.pop(title, None)


def book_genre_edit(title, new_genre):
    if title not in book_dict:
        print('You don\'t have this Book in your library.'
              'You can see all your books by command \"book_all()\"')
    elif new_genre not in genre_dict:
        print('You don\'t have this Genre in your library.'
              'You can see all your Genres by command \"genre_all()\"')
    else:
        book_dict[title].genre = new_genre
        print('Genre of this Book was changed')


def main():
    if len(sys.argv) == 1:
        help_me()
        sys.exit(1)
    else:
        option = sys.argv[1]
        if option == '--add-genre':
            local_data.genre_from_file()
            genre_title = sys.argv[2]
            genre_add(genre_title)
            in_file.genre_to_file()


if __name__ == '__main__':
    main()
