import modules


def genre_add(title, id):
    new_genre = modules.genre.Genre(id, title)
    genre_dict[new_genre.genre_id] = new_genre


genre_dict = {}


def genre_from_file():
    with open("db/in_file/genres.txt", 'r') as fh:
        data = fh.read().split()
        val1 = 'id:'
        val2 = 'title:'
        while val1 in data:
            data.remove(val1)
        while val2 in data:
            data.remove(val2)
        id = data[::2]
        genre = data[1::2]
        j = 0
        for i in genre:
            genre_add(i, id[j])
            j += 1

# book1 = book.Book(id_generator.generate_id(), 'Harry Potter', 'J. K. Rowling', 1997, 'About magic', 'Fantasy')
# book2 = book.Book(id_generator.generate_id(), 'The Lord of the Rings', 'J. R. R. Tolkien', 1954, 'About magic world',
#                   'Fantasy')
# book3 = book.Book(id_generator.generate_id(), 'Programming Python, 4th Edition', 'Mark Lutz', 2010,
#                   'Powerful Object-Oriented Programming', 'Interesting')
#
# book_dict = {}
#
# book_dict[book1.title] = book1
# book_dict[book2.title] = book2
# book_dict[book3.title] = book3
