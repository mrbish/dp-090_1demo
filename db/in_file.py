from local_data import local_data


def genre_to_file():
    with open("db/in_file/genres.txt", "w") as f:
        for key in local_data.genre_dict:
            f.write(str(local_data.genre_dict[key]) + '\n')
