local_init:
	sudo apt-get update --fix-missing -y
	sudo apt-get install python-pip -y
	sudo pip install virtualenv

init:
    pip install -r requirements.txt