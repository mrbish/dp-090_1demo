from local_data import local_data


def genre_info(title):
    """information about genre"""
    id = [key for key, value in local_data.genre_dict.items() if local_data.genre_dict[key].title == title]
    print(local_data.genre_dict[id])


def genre_all():
    for key, value in local_data.genre_dict.items():
        print(local_data.genre_dict[key].title)


def book_all_by_genre(genre_title):
    titles = [local_data.genre_dict[key].title for key, value in local_data.genre_dict.items()]
    if genre_title not in titles:
        print('You don\'t have this Genre in your library.'
              'You can see all your Genres by command \"genre_all()\"')
    else:
        for key, val in local_data.book_dict.items():
            if val.genre == genre_title:
                print('=============')
                print(val)
