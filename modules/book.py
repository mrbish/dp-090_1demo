class Book(object):
    """docstring for Book"""

    def __init__(self, book_id, title, author, year, description, genre):
        self.__book_id = book_id
        self.__title = title
        self.__author = author
        self.__year = year
        self.__description = description
        self.__genre = genre

    @property
    def genre(self):
        return self.__genre

    @genre.setter
    def genre(self, new_genre):
        self.__genre = new_genre.title()

    @property
    def title(self):
        return self.__title

    @property
    def author(self):
        return self.__author

    @property
    def year(self):
        return self.__year

    @property
    def description(self):
        return self.__description

    def __str__(self):
        return 'Title: {0.title} \nAuthor: {0.author} \nYear: {0.year} \nDescription: ' \
               '{0.description} \nGenre: {0.genre}'.format(self)
