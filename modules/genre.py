class Genre(object):
    """docstring for Genre"""

    def __init__(self, genre_id, title):
        self.__genre_id = genre_id
        self.__title = title

    @property
    def genre_id(self):
        return self.__genre_id

    @property
    def title(self):
        return self.__title

    # @title.setter
    # def title(self, new_title):
    #     self.__title = new_title.title()

    def __str__(self):
        return 'id: {genre_id} title: {title}'.format(genre_id=self.genre_id, title=self.title)
